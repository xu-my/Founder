import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.jasypt.util.text.BasicTextEncryptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
	public static void main(String args[]) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		//加密所需的salt(盐)
		textEncryptor.setPassword("founder");
		//要加密的数据（数据库的用户名或密码）
		String username = textEncryptor.encrypt("founder");
		String password = textEncryptor.encrypt("founder");
		System.out.println("username:"+username);
		System.out.println("password:"+password);

		//System.out.println(textEncryptor.decrypt("imENTO7M8bLO38LFSIxnzw=="));
		//System.out.println(textEncryptor.decrypt("i3cDFhs26sa2Ucrfz2hnQw=="));
	}

	@org.junit.Test
	public void testH() {
		JSONObject obj = new JSONObject();
		obj.put("key1", new ArrayList());
		obj.put("key2", new ArrayList());
		obj.put("key3", new ArrayList());
		Map<String, List<Object>> params = JSONObject.parseObject(obj.toJSONString(),
				new TypeReference<Map<String, List<Object>>>(){});
		System.out.println(params);
	}
	@org.junit.Test
	public void testM() {
		JSONArray a = new JSONArray();
		HashMap<String, ArrayList> h = new HashMap();
		h.put("hehe", new ArrayList());
		a.add(h);
		List<HashMap<String,ArrayList>> list = JSONObject.parseObject(a.toJSONString(),
				new TypeReference<List<HashMap<String,ArrayList>>>(){});
		System.out.println(list);
	}
}
