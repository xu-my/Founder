package cn.com.founder.admin;

import cn.com.founder.common.security.annotation.EnableFounderFeignClients;
import cn.com.founder.common.security.annotation.EnableFounderResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableFounderResourceServer
@EnableFounderFeignClients
@SpringCloudApplication
public class FounderAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(FounderAdminApplication.class, args);
	}

}
