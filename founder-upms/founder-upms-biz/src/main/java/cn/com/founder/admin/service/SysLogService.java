package cn.com.founder.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.com.founder.admin.api.entity.SysLog;

/**
* 日志表 服务类
*/
public interface SysLogService extends IService<SysLog> {

}
