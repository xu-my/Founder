package cn.com.founder.admin.service.impl;

import cn.com.founder.admin.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.founder.admin.api.entity.SysUserRole;
import cn.com.founder.admin.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* 用户角色表 服务实现类
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

	/**
	 * 根据用户Id删除该用户的角色关系
	 * @param userId 用户ID
	 * @return boolean
	 */
	@Override
	public Boolean removeRoleByUserId(Integer userId) {
		return baseMapper.deleteByUserId(userId);
	}

}
