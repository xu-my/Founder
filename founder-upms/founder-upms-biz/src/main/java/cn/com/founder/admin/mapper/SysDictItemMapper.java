package cn.com.founder.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.admin.api.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典项
*/
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
