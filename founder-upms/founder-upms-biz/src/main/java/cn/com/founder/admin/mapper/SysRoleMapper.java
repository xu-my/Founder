package cn.com.founder.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.admin.api.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* Mapper 接口
*/
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

	/**
	 * 通过用户ID，查询角色信息
	 * @param userId
	 * @return
	 */
	List<SysRole> listRolesByUserId(Integer userId);

}
