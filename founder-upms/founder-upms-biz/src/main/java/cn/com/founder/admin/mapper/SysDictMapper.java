package cn.com.founder.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.admin.api.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;

/**
* 字典表 Mapper 接口
*/
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
