package cn.com.founder.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.admin.api.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;

/**
* 日志表 Mapper 接口
*/
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {

}
