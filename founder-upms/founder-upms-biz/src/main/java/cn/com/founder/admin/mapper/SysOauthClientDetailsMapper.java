package cn.com.founder.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.admin.api.entity.SysOauthClientDetails;
import org.apache.ibatis.annotations.Mapper;

/**
* Mapper 接口
*/
@Mapper
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
