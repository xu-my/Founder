package cn.com.founder.admin.service.impl;

import cn.com.founder.admin.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.founder.admin.api.entity.SysLog;
import cn.com.founder.admin.mapper.SysLogMapper;
import org.springframework.stereotype.Service;

/**
* 日志表 服务实现类
*/
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
