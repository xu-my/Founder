package cn.com.founder.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.admin.api.entity.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
* 角色菜单表 Mapper 接口
*/
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
