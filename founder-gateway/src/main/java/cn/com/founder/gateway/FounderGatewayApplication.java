package cn.com.founder.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * 网关应用
 */
@SpringCloudApplication
public class FounderGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(FounderGatewayApplication.class, args);
	}

}
