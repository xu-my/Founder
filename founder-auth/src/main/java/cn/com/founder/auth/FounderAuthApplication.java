package cn.com.founder.auth;

import cn.com.founder.common.security.annotation.EnableFounderFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
@EnableFounderFeignClients
public class FounderAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(FounderAuthApplication.class, args);
	}

}
