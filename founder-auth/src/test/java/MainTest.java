import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class MainTest {
	public static void main(String args[]) {
		String pw = BCrypt.hashpw("123456", BCrypt.gensalt());
		System.out.println(pw);
		boolean flag = BCrypt.checkpw("123456", "$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC");
		System.out.println(flag);
	}
}
