package com.xxl.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FounderXxlJobAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(FounderXxlJobAdminApplication.class, args);
	}

}