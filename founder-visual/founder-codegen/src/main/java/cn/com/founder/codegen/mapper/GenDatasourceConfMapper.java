package cn.com.founder.codegen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.com.founder.codegen.entity.GenDatasourceConf;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据源表
*/
@Mapper
public interface GenDatasourceConfMapper extends BaseMapper<GenDatasourceConf> {

}
