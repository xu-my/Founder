package cn.com.founder.codegen;

import cn.com.founder.common.datasource.annotation.EnableDynamicDataSource;
import cn.com.founder.common.security.annotation.EnableFounderFeignClients;
import cn.com.founder.common.security.annotation.EnableFounderResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableDynamicDataSource
@EnableFounderFeignClients
@SpringCloudApplication
@EnableFounderResourceServer
public class FounderCodeGenApplication {

	public static void main(String[] args) {
		SpringApplication.run(FounderCodeGenApplication.class, args);
	}

}
