package cn.com.founder.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableAdminServer
@SpringCloudApplication
public class FounderMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(FounderMonitorApplication.class, args);
	}

}
