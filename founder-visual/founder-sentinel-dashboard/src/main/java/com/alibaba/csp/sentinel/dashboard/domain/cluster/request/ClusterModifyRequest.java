package com.alibaba.csp.sentinel.dashboard.domain.cluster.request;

public interface ClusterModifyRequest {

	String getApp();

	String getIp();

	Integer getPort();

	Integer getMode();

}
