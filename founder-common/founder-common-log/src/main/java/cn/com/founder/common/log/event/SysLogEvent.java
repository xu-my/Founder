package cn.com.founder.common.log.event;

import cn.com.founder.admin.api.entity.SysLog;
import org.springframework.context.ApplicationEvent;

public class SysLogEvent extends ApplicationEvent {

	public SysLogEvent(SysLog source) {
		super(source);
	}

}
