package cn.com.founder.common.core.constant;

public interface ServiceNameConstants {

	/**
	 * 认证服务的SERVICEID
	 */
	String AUTH_SERVICE = "founder-auth";

	/**
	 * UMPS模块
	 */
	String UMPS_SERVICE = "founder-upms-biz";

}
