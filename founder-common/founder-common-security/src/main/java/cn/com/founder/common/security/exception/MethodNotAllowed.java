package cn.com.founder.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cn.com.founder.common.security.component.FounderAuth2ExceptionSerializer;
import org.springframework.http.HttpStatus;

@JsonSerialize(using = FounderAuth2ExceptionSerializer.class)
public class MethodNotAllowed extends FounderAuth2Exception {

	public MethodNotAllowed(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "method_not_allowed";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.METHOD_NOT_ALLOWED.value();
	}

}
