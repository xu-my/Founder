package cn.com.founder.common.security.annotation;

import cn.com.founder.common.security.component.FounderResourceServerAutoConfiguration;
import cn.com.founder.common.security.component.FounderSecurityBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import java.lang.annotation.*;

/**
 * 资源服务注解
 */
@Documented
@Inherited
@org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import({ FounderResourceServerAutoConfiguration.class, FounderSecurityBeanDefinitionRegistrar.class })
public @interface EnableFounderResourceServer {

}
