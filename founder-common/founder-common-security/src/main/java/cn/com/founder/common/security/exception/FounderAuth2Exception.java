package cn.com.founder.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cn.com.founder.common.security.component.FounderAuth2ExceptionSerializer;
import lombok.Getter;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

@JsonSerialize(using = FounderAuth2ExceptionSerializer.class)
public class FounderAuth2Exception extends OAuth2Exception {

	@Getter
	private String errorCode;

	public FounderAuth2Exception(String msg) {
		super(msg);
	}

	public FounderAuth2Exception(String msg, String errorCode) {
		super(msg);
		this.errorCode = errorCode;
	}

}
