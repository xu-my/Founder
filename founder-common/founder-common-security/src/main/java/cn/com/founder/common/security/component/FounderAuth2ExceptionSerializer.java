package cn.com.founder.common.security.component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import cn.com.founder.common.core.constant.CommonConstants;
import cn.com.founder.common.security.exception.FounderAuth2Exception;
import lombok.SneakyThrows;

/**
 * OAuth2 异常格式化
 */
public class FounderAuth2ExceptionSerializer extends StdSerializer<FounderAuth2Exception> {

	public FounderAuth2ExceptionSerializer() {
		super(FounderAuth2Exception.class);
	}

	@Override
	@SneakyThrows
	public void serialize(FounderAuth2Exception value, JsonGenerator gen, SerializerProvider provider) {
		gen.writeStartObject();
		gen.writeObjectField("code", CommonConstants.FAIL);
		gen.writeStringField("msg", value.getMessage());
		gen.writeStringField("data", value.getErrorCode());
		gen.writeEndObject();
	}

}
