package cn.com.founder.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cn.com.founder.common.security.component.FounderAuth2ExceptionSerializer;
import org.springframework.http.HttpStatus;

@JsonSerialize(using = FounderAuth2ExceptionSerializer.class)
public class ServerErrorException extends FounderAuth2Exception {

	public ServerErrorException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "server_error";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR.value();
	}

}
