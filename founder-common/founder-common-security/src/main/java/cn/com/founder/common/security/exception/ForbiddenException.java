package cn.com.founder.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cn.com.founder.common.security.component.FounderAuth2ExceptionSerializer;
import org.springframework.http.HttpStatus;

@JsonSerialize(using = FounderAuth2ExceptionSerializer.class)
public class ForbiddenException extends FounderAuth2Exception {

	public ForbiddenException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "access_denied";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.FORBIDDEN.value();
	}

}
