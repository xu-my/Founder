package cn.com.founder.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cn.com.founder.common.security.component.FounderAuth2ExceptionSerializer;

@JsonSerialize(using = FounderAuth2ExceptionSerializer.class)
public class InvalidException extends FounderAuth2Exception {

	public InvalidException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "invalid_exception";
	}

	@Override
	public int getHttpErrorCode() {
		return 426;
	}

}
